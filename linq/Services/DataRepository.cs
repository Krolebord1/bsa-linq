﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linq.DTOs;
using Linq.Entities;
using Task = System.Threading.Tasks.Task;

namespace Linq.Services
{
    public class DataRepository : IDataRepository
    {
        private List<Project>? _projects;
        private List<Entities.Task>? _tasks;
        private List<Team>? _teams;
        private List<User>? _users;

        public async Task<IReadOnlyList<Project>> GetProjectsAsync()
        {
            if (_projects == null)
                await ResolveData();

            return _projects!;
        }

        public async Task<IReadOnlyList<Entities.Task>> GetTasksAsync()
        {
            if (_tasks == null)
                await ResolveData();

            return _tasks!;
        }

        public async Task<IReadOnlyList<Team>> GetTeamsAsync()
        {
            if (_teams == null)
                await ResolveData();

            return _teams!;
        }

        public async Task<IReadOnlyList<User>> GetUsersAsync()
        {
            if (_users == null)
                await ResolveData();

            return _users!;
        }

        private async Task ResolveData()
        {
            using var dataLoader = DI.GetService<IDataLoader>();

            var projectsLoadTask = dataLoader.GetProjectDTOsAsync();
            var tasksLoadTask = dataLoader.GetTaskDTOsAsync();
            var teamsLoadTask = dataLoader.GetTeamDTOsAsync();
            var usersLoadTask = dataLoader.GetUserDTOsAsync();

            await Task.WhenAll(projectsLoadTask, tasksLoadTask, teamsLoadTask, usersLoadTask);

            List<ProjectReadDTO> projectDTOs = projectsLoadTask.Result;
            List<TaskReadDTO> taskDTOs = tasksLoadTask.Result;
            List<TeamReadDTO> teamsDTOs = teamsLoadTask.Result;
            List<UserReadDTO> userDTOs = usersLoadTask.Result;

            _tasks = taskDTOs.Select(dto => dto.ToTask()).ToList();

            _teams = teamsDTOs.Select(dto => dto.ToTeam()).ToList();

            _users = userDTOs
                .Select(dto => dto.ToUser())
                .Join(
                    _teams,
                    user => user.TeamId,
                    team => team.Id,
                    (user, team) =>
                    {
                        team.Users.Add(user);
                        user.Team = team;
                        return user;
                    }
                )
                .GroupJoin(
                    _tasks,
                    user => user.Id,
                    task => task.PerformerId,
                    (user, userTasks) =>
                    {
                        user.Tasks = userTasks
                            .Select(task =>
                            {
                                task.Performer = user;
                                return task;
                            })
                            .ToList();
                        return user;
                    }
                )
                .ToList();

            _projects = projectDTOs
                .Select(projectDTO => projectDTO.ToProject())
                .Join(
                    _users,
                    project => project.AuthorId,
                    user => user.Id,
                    (project, user) =>
                    {
                        project.Author = user;
                        return project;
                    }
                )
                .Join(
                    _teams,
                    project => project.TeamId,
                    team => team.Id,
                    (project, team) =>
                    {
                        team.Project = project;
                        project.Team = team;
                        return project;
                    }
                )
                .GroupJoin(
                    _tasks,
                    project => project.Id,
                    task => task.ProjectId,
                    (project, projectTasks) =>
                    {
                        project.Tasks = projectTasks
                            .Select(task =>
                            {
                                task.Project = project;
                                return task;
                            })
                            .ToList();
                        return project;
                    }
                )
                .ToList();
        }
    }
}
