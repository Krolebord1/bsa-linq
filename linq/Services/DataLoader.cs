﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Linq.DTOs;
using Newtonsoft.Json;

namespace Linq.Services
{
    public class DataLoader : IDataLoader
    {
        private readonly HttpClient _client;

        public DataLoader()
        {
            _client = new HttpClient();
        }

        public async Task<List<ProjectReadDTO>> GetProjectDTOsAsync()
        {
            return await GetDeserializedAsync<List<ProjectReadDTO>>(ApiUrls.AllProjects()) ?? new();
        }

        public async Task<List<TaskReadDTO>> GetTaskDTOsAsync()
        {
            return await GetDeserializedAsync<List<TaskReadDTO>>(ApiUrls.AllTasks()) ?? new();
        }

        public async Task<List<TeamReadDTO>> GetTeamDTOsAsync()
        {
            return await GetDeserializedAsync<List<TeamReadDTO>>(ApiUrls.AllTeams()) ?? new();
        }

        public async Task<List<UserReadDTO>> GetUserDTOsAsync()
        {
            return await GetDeserializedAsync<List<UserReadDTO>>(ApiUrls.AllUsers()) ?? new();
        }

        private async Task<T?> GetDeserializedAsync<T>(string uri)
        {
            var content = await GetContentAsync(uri);

            var json = await content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(json);
        }

        private async Task<HttpContent> GetContentAsync(string uri)
        {
            try
            {
                var response = await _client.GetAsync(uri);

                if (!response.IsSuccessStatusCode)
                    throw new DataLoadingException($"Response status code is: {response.StatusCode}");

                return response.Content;
            }
            catch (HttpRequestException e)
            {
                throw new DataLoadingException("HttpException has been thrown", e);
            }
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }

    public class DataLoadingException : Exception
    {
        public DataLoadingException(string message, HttpRequestException? httpException = default)
            : base(message, httpException) {}
    }
}
