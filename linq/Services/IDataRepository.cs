﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Linq.Entities;

namespace Linq.Services
{
    public interface IDataRepository
    {
        public Task<IReadOnlyList<Project>> GetProjectsAsync();

        public Task<IReadOnlyList<Entities.Task>> GetTasksAsync();

        public Task<IReadOnlyList<Team>> GetTeamsAsync();

        public Task<IReadOnlyList<User>> GetUsersAsync();
    }
}
