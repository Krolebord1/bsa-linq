﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Linq.Entities;

namespace Linq.Services
{
    public interface IDataProcessor
    {
        Task<IDictionary<Project, int>> GetUserTasksCount(int userId);

        Task<IList<Entities.Task>> GetUserTasks(int userId);

        Task<IList<(int, string)>> GetUserTasksFinishedInCurrentYear(int userId);

        Task<IList<(int, string, List<User>)>> GetTeamsWithOldEnoughUsers();

        Task<IList<User>> GetSortedUsers();

        Task<UserSummary> GetUserSummary(int userId);

        Task<IList<ProjectSummary>> GetProjectSummary();
    }
}
