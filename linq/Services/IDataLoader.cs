﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Linq.DTOs;

namespace Linq.Services
{
    public interface IDataLoader : IDisposable
    {
        public Task<List<ProjectReadDTO>> GetProjectDTOsAsync();

        public Task<List<TaskReadDTO>> GetTaskDTOsAsync();

        public Task<List<TeamReadDTO>> GetTeamDTOsAsync();

        public Task<List<UserReadDTO>> GetUserDTOsAsync();
    }
}
