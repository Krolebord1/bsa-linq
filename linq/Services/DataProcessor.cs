﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linq.Entities;
using Task = Linq.Entities.Task;

namespace Linq.Services
{
    public class DataProcessor : IDataProcessor
    {
        private const int MaxTaskLength = 45;

        private const int OldEnoughAge = 10;

        private const int MinProjectDescriptionLength = 20;

        private const int MaxProjectTasksCount = 3;

        private readonly IDataRepository _repository;

        public DataProcessor(IDataRepository repository)
        {
            _repository = repository;
        }

        // Method 1
        // Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public async Task<IDictionary<Project, int>> GetUserTasksCount(int userId)
        {
            var projects = await _repository.GetProjectsAsync();

            return projects.ToDictionary(
                project => project,
                project => project.Tasks.Count(task => task.PerformerId == userId)
            );
        }

        // Method 2
        // Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        public async Task<IList<Task>> GetUserTasks(int userId)
        {
            var users = await _repository.GetUsersAsync();

            return users
                .FirstOrDefault(user => user.Id == userId)?
                .Tasks.Where(task => task.Name.Length < MaxTaskLength).ToList() ?? new ();
        }

        // Method 3
        // Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).
        public async Task<IList<(int, string)>> GetUserTasksFinishedInCurrentYear(int userId)
        {
            var currentYear = DateTime.Now.Year;

            var tasks = await _repository.GetTasksAsync();

            return tasks
                .Where(task =>
                    task.PerformerId == userId && task.FinishedAt != null && task.FinishedAt.Value.Year == currentYear)
                .Select(task => (task.Id, task.Name)).ToList();
        }

        // Method 4
        // Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        public async Task<IList<(int, string, List<User>)>> GetTeamsWithOldEnoughUsers()
        {
            int maxBirthYear = DateTime.Now.Year - OldEnoughAge - 1;

            var teams = await _repository.GetTeamsAsync();

            return teams
                .Select(
                    team => (
                        team.Id,
                        team.Name,
                        team.Users.Where(user => user.BirthDay.Year < maxBirthYear).ToList()
                    )
                )
                .ToList();
        }

        // Method 5
        // Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public async Task<IList<User>> GetSortedUsers()
        {
            var users = await _repository.GetUsersAsync();

            return users
                .OrderBy(user => user.FirstName)
                .Select(user =>
                {
                    user.Tasks = user.Tasks.OrderByDescending(task => task.Name).ToList();
                    return user;
                })
                .ToList();
        }

        // Method 6
        public async Task<UserSummary> GetUserSummary(int userId)
        {
            var users = await _repository.GetUsersAsync();

            return users
                .Where(user => user.Id == userId)
                .Take(1)
                .Select(user =>
                {
                    var lastProject = user.Tasks
                        .Where(task => task.FinishedAt != null)
                        .Aggregate((taskA, taskB) =>
                            DateTime.Compare(taskA.FinishedAt!.Value, taskB.FinishedAt!.Value) < 0
                                ? taskA
                                : taskB
                        )
                        .Project;

                    var longestTask = user.Tasks.Where(task => task.FinishedAt != null)
                        .Aggregate((taskA, taskB) =>
                            taskA.FinishedAt!.Value.Ticks - taskA.CreatedAt.Ticks > taskB.FinishedAt!.Value.Ticks - taskB.CreatedAt.Ticks
                                ? taskA
                                : taskB
                        );

                    return new UserSummary(
                        user,
                        lastProject,
                        user.Tasks.Count(task => task.ProjectId == lastProject.Id),
                        user.Tasks.Count(task => task.FinishedAt == null),
                        longestTask
                    );
                })
                .FirstOrDefault();
        }

        // Method 7
        public async Task<IList<ProjectSummary>> GetProjectSummary()
        {
            var projects = await _repository.GetProjectsAsync();

            return projects
                .Select(project =>
                {
                    Task? longestTask = !project.Tasks.Any() ? null :
                        project.Tasks.Aggregate(
                            (taskA, taskB) => taskA.Description.Length > taskB.Description.Length ? taskA : taskB);

                    Task? shortestTask = !project.Tasks.Any() ? null :
                        project.Tasks.Aggregate(
                            (taskA, taskB) => taskA.Name.Length < taskB.Name.Length ? taskA : taskB);

                    int? teamCount =
                        project.Description.Length > MinProjectDescriptionLength ||
                        project.Tasks.Count < MaxProjectTasksCount
                            ? project.Team.Users.Count
                            : null;

                    return new ProjectSummary(
                        project,
                        longestTask,
                        shortestTask,
                        teamCount
                    );
                })
                .ToList();
        }
    }
}
