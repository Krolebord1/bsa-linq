﻿using System.Threading.Tasks;

namespace Linq.Interfaces
{
    public interface IAsyncProgram
    {
        public Task RunAsync();
    }
}
