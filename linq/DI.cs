﻿using System;
using Linq.Interfaces;
using Linq.Programs;
using Linq.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Linq
{
    public static class DI
    {
        private static readonly IServiceProvider Provider;

        static DI()
        {
            var services = new ServiceCollection();

            services.AddTransient<IDataLoader, DataLoader>();

            services.AddSingleton<IDataRepository, DataRepository>();
            services.AddSingleton<IDataProcessor, DataProcessor>();
            services.AddSingleton<IAsyncProgram, MainProgram>();

            Provider = services.BuildServiceProvider();
        }

        public static T GetService<T>() =>
            Provider.GetService<T>() ?? throw new DependencyInjectionException(typeof(T));
    }

    public class DependencyInjectionException : Exception
    {
        public DependencyInjectionException(Type type)
            : base($"Type <{type.FullName} is not registered inside DI container>") {}
    }
}
