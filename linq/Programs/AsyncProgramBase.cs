﻿using System;
using System.Threading.Tasks;
using Linq.Interfaces;

namespace Linq.Programs
{
    public abstract class AsyncProgramBase : IAsyncProgram
    {
        public abstract Task RunAsync();

        protected void WaitForKey()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        protected void WriteError(string message = "")
        {
            Console.WriteLine();
            Console.WriteLine("Error occurred");

            if(!string.IsNullOrWhiteSpace(message))
                Console.WriteLine($"Message: \n{message}");

            WaitForKey();
        }
    }
}
