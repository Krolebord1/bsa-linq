﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linq.Interfaces;

namespace Linq.Programs
{
    public class NavigatorProgram : IAsyncProgram
    {
        private readonly List<NavigatorEntry> _entries;

        public NavigatorProgram(IEnumerable<KeyValuePair<string, IAsyncProgram>> programs)
        {
            _entries = programs
                .Select((kvp, index) => new NavigatorEntry(index, kvp.Key, kvp.Value))
                .ToList();
        }

        public async Task RunAsync()
        {
            while(true)
            {
                Console.Clear();
                WriteCatalog();

                var line = Console.ReadLine();

                if(line == null)
                    continue;

                string argument = line.ToLower();

                if(string.IsNullOrWhiteSpace(argument))
                    break;

                var entry = int.TryParse(argument, out int index)
                    ? _entries.Find(x => x.index == index-1)
                    : _entries.Find(x => x.path.ToLower() == argument);

                if (entry == null)
                    continue;

                Console.Title = entry.path;
                Console.Clear();

                await entry.program.RunAsync();

                Console.Title = Program.AppTitle;
            }
        }

        private void WriteCatalog()
        {
            Console.WriteLine("Select action: ");
            foreach (NavigatorEntry entry in _entries)
                Console.WriteLine($"{entry.index+1}. {entry.path}");

            Console.WriteLine("Or press enter to go back.");
        }

        private class NavigatorEntry
        {
            public readonly int index;
            public readonly string path;
            public readonly IAsyncProgram program;

            public NavigatorEntry(int index, string path, IAsyncProgram program)
            {
                this.index = index;
                this.path = path;
                this.program = program;
            }
        }
    }
}
