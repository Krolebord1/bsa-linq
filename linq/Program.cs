﻿using System;
using System.Threading.Tasks;
using Linq.Interfaces;

namespace Linq
{
    static class Program
    {
        public const string AppTitle = "BSA 2021 Task 1";

        static async Task Main()
        {
            Console.Title = AppTitle;

            var app = DI.GetService<IAsyncProgram>();
            await app.RunAsync();
        }
    }
}
