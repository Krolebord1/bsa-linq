﻿using System;
using System.Collections.Generic;

namespace Linq.Entities
{
    public record User
    {
        public int Id { get; init; }

        public string FirstName { get; init; } = string.Empty;

        public string LastName { get; init; } = string.Empty;

        public string Email { get; init; } = string.Empty;

        public DateTime RegisteredAt { get; init; }

        public DateTime BirthDay { get; init; }

        public int? TeamId { get; set; }
        public Team? Team { get; set; }

        public IList<Task> Tasks { get; set; } = new List<Task>();

        public string FullName => $"{FirstName} {LastName}";
    }
}
