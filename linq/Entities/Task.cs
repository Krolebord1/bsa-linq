﻿using System;
using Linq.Enums;

namespace Linq.Entities
{
    public class Task
    {
        public int Id { get; init; }

        public string Name { get; init; } = string.Empty;

        public string Description { get; init; } = string.Empty;

        public TaskState State { get; init; }

        public DateTime CreatedAt { get; init; }

        public DateTime? FinishedAt { get; init; }

        public int ProjectId { get; set; }
        public Project Project { get; set; } = default!;

        public int PerformerId { get; set; }
        public User Performer { get; set; } = default!;
    }
}
