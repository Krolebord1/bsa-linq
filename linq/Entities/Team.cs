﻿using System;
using System.Collections.Generic;

namespace Linq.Entities
{
    public record Team
    {
        public int Id { get; init; }

        public string Name { get; init; } = string.Empty;

        public DateTime CreatedAt { get; init; }

        public Project Project { get; set; } = default!;

        public IList<User> Users { get; init; } = new List<User>();
    }
}
