﻿using System;
using System.Collections.Generic;

namespace Linq.Entities
{
    public class Project
    {
        public int Id { get; init; }

        public string Name { get; init; } = "";

        public string Description { get; init; } = "";

        public DateTime Deadline { get; init; }

        public DateTime CreatedAt { get; init; }

        public int AuthorId { get; set; }
        public User Author { get; set; } = default!;
        public int TeamId { get; set; }
        public Team Team { get; set; } = default!;

        public IList<Task> Tasks { get; set; } = new List<Task>();
    }
}
