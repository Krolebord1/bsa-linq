﻿namespace Linq.Entities
{
    public readonly struct UserSummary
    {
        public readonly User user;

        public readonly Project lastProject;

        public readonly int lastProjectTaskCount;

        public readonly int unfinishedTasksCount;

        public readonly Task longestUnfinishedTask;

        public UserSummary(User user, Project lastProject, int lastProjectTaskCount, int unfinishedTasksCount, Task longestUnfinishedTask)
        {
            this.user = user;
            this.lastProject = lastProject;
            this.lastProjectTaskCount = lastProjectTaskCount;
            this.unfinishedTasksCount = unfinishedTasksCount;
            this.longestUnfinishedTask = longestUnfinishedTask;
        }

        public override string ToString() =>
            $"User: {user.FullName}" +
            $"\n\tLast project: {lastProject.Name}" +
            $"\n\tLast project tasks count: {lastProjectTaskCount}" +
            $"\n\tUnfinished tasks count: {unfinishedTasksCount}" +
            $"\n\tLongest unfinished task: {longestUnfinishedTask.Name}";
    }
}
