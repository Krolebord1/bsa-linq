﻿namespace Linq.Entities
{
    public readonly struct ProjectSummary
    {
        public readonly Project project;

        public readonly Task? longestTask;

        public readonly Task? shortestTask;

        public readonly int? usersCount;

        public ProjectSummary(Project project, Task? longestTask, Task? shortestTask, int? usersCount)
        {
            this.project = project;
            this.longestTask = longestTask;
            this.shortestTask = shortestTask;
            this.usersCount = usersCount;
        }

        public override string ToString() =>
            $"Project: {project.Name}" +
            $"\n\tLongest task: {longestTask?.Name ?? "none"}" +
            $"\n\tShortest task: {shortestTask?.Name ?? "none"}" +
            (usersCount != null ? $"\n\tUsers count: {usersCount.ToString()}" : string.Empty);
    }
}
