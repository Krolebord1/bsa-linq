﻿using System;
using Linq.Entities;

namespace Linq.DTOs
{
    public record UserReadDTO(
        int Id,
        int? TeamId,
        string? FirstName,
        string? LastName,
        string? Email,
        DateTime RegisteredAt,
        DateTime BirthDay
    );

    public static class UserDTOExtensions
    {
        public static User ToUser(this UserReadDTO dto) =>
            new()
            {
                Id = dto.Id,
                FirstName = dto.FirstName ?? string.Empty,
                LastName = dto.LastName ?? string.Empty,
                Email = dto.Email ?? string.Empty,
                RegisteredAt = dto.RegisteredAt,
                BirthDay = dto.BirthDay,
                TeamId = dto.TeamId
            };
    }
}
