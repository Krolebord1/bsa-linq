﻿using System;
using Linq.Entities;

namespace Linq.DTOs
{
    public record TeamReadDTO(
        int Id,
        string? Name,
        DateTime CreatedAt
    );

    public static class TeamDTOExtensions
    {
        public static Team ToTeam(this TeamReadDTO dto) =>
            new()
            {
                Id = dto.Id,
                Name = dto.Name ?? string.Empty,
                CreatedAt = dto.CreatedAt
            };
    }
}
