﻿using System;
using Linq.Entities;
using Linq.Enums;

namespace Linq.DTOs
{
    public record TaskReadDTO(
        int Id,
        int ProjectId,
        int PerformerId,
        string? Name,
        string? Description,
        TaskState State,
        DateTime CreatedAt,
        DateTime? FinishedAt
    );

    public static class TaskDTOExtensions
    {
        public static Task ToTask(this TaskReadDTO dto) =>
            new()
            {
                Id = dto.Id,
                Name = dto.Name ?? string.Empty,
                Description = dto.Description ?? string.Empty,
                State = dto.State,
                CreatedAt = dto.CreatedAt,
                FinishedAt = dto.FinishedAt,
                ProjectId = dto.ProjectId,
                PerformerId = dto.PerformerId
            };
    }
}
