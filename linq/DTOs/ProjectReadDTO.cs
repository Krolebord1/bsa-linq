﻿using System;
using Linq.Entities;

namespace Linq.DTOs
{
    public record ProjectReadDTO(
        int Id,
        int AuthorId,
        int TeamId,
        string? Name,
        string? Description,
        DateTime Deadline,
        DateTime CreatedAt
    );

    public static class ProjectDTOExtensions
    {
        public static Project ToProject(this ProjectReadDTO dto) =>
            new()
            {
                Id = dto.Id,
                Name = dto.Name ?? string.Empty,
                Description = dto.Description ?? string.Empty,
                Deadline = dto.Deadline,
                CreatedAt = dto.CreatedAt,
                AuthorId = dto.AuthorId,
                TeamId = dto.TeamId
            };
    }
}
