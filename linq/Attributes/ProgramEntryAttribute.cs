﻿using System;

namespace Linq.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class ProgramEntryAttribute : Attribute
    {
        public string Path { get; }

        public ProgramEntryAttribute(string path)
        {
            Path = path;
        }
    }
}
