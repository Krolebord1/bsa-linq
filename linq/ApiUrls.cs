﻿namespace Linq
{
    public static class ApiUrls
    {
        public static readonly string Scheme = "https";
        public static readonly string Authority = "bsa21.azurewebsites.net";
        public static readonly string ApiPath = "api";

        public static readonly string FullApiPath = $"{Scheme}://{Authority}/{ApiPath}";

        public static string AllProjects() => $"{FullApiPath}/Projects";
        public static string SpecifiedProject(int id) => $"{FullApiPath}/Projects/{id}";

        public static string AllTasks() => $"{FullApiPath}/Tasks";
        public static string SpecifiedTask(int id) => $"{FullApiPath}/Tasks/{id}";

        public static string AllTeams() => $"{FullApiPath}/Teams";
        public static string SpecifiedTeam(int id) => $"{FullApiPath}/Team/{id}";

        public static string AllUsers() => $"{FullApiPath}/Users";
        public static string SpecifiedUser(int id) => $"{FullApiPath}/Users/{id}";
    }
}
